This repository contains the modified ODE and Box2D physics engines used for the following paper:

Perttu Hämäläinen, Xiaoxiao Ma, Jari Takatalo, Julian Togelius. 2017. Predictive Physics Simulation in Game Mechanics. Proc. CHI PLAY 2017. ACM Press.

A preprint of the paper is provided as a pdf, paper_chiplay2017-hamalainen.pdf in the same folder as this readme.

If you use the code, please cite the paper.

In addition to the original ODE and Box2D licenses and readmes, please note the following disclaimer:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

