#include "TrailPrediction.h"



//those are global for now for test of multithread
Settings currentSettings;
int currentStep = 0;
int currentThreadStep = 0;
int bombShootStep = -1;
int bombSpawnStep = -1;
std::vector<std::vector<b2Vec2>> trails;
b2Body* bombCopied = NULL;
WorldDataSet datasetCopy;
//////////////////////////
//bomb related//////////
bool mBombSpawning;
b2Vec2 mBmbSpawnPos;
b2Vec2 mBmbSpawnMousePos;
int TestLoopCount = 1000;

bool loopThread = true;

//mutex for the trails record
std::mutex mu;


void threadTest2(b2World& w) {
	while (loopThread) {
		while (currentThreadStep < currentStep) {

			if (currentThreadStep == bombShootStep) {
				float multiplier = 5;
				TrailPrediction::updateProjectile(mBmbSpawnPos, multiplier*(mBmbSpawnPos - mBmbSpawnMousePos), bombCopied);
			}
			else if (currentThreadStep == bombSpawnStep) {
				if (bombCopied == NULL) {
					bombCopied = TrailPrediction::createProjectile(mBmbSpawnPos, b2Vec2_zero, &w);
				}
				else {
					TrailPrediction::updateProjectile(mBmbSpawnPos, b2Vec2_zero, bombCopied);
				}
			}


			float32 timeStep = currentSettings.hz > 0.0f ? 1.0f / currentSettings.hz : float32(0.0f);
			w.SetAllowSleeping(currentSettings.enableSleep);
			w.SetWarmStarting(currentSettings.enableWarmStarting);
			w.SetContinuousPhysics(currentSettings.enableContinuous);
			w.SetSubStepping(currentSettings.enableSubStepping);
			w.Step(timeStep, currentSettings.velocityIterations, currentSettings.positionIterations);
			currentThreadStep++;

			if (currentThreadStep == bombShootStep) {
				float multiplier = 5;
				TrailPrediction::updateProjectile(mBmbSpawnPos, multiplier*(mBmbSpawnPos - mBmbSpawnMousePos), bombCopied);
			}
			else if (currentThreadStep == bombSpawnStep) {
				if (bombCopied == NULL) {
					bombCopied = TrailPrediction::createProjectile(mBmbSpawnPos, b2Vec2_zero, &w);
				}
				else {
					TrailPrediction::updateProjectile(mBmbSpawnPos, b2Vec2_zero, bombCopied);
				}
			}
			
		}


		w.snapshot();
		if (bombCopied != NULL && mBombSpawning)
		{
			float multiplier = 5;
			TrailPrediction::updateProjectile(mBmbSpawnPos, multiplier*(mBmbSpawnPos - mBmbSpawnMousePos), bombCopied);
		}

		for (auto &t : trails) {
			t.clear();
		}

		for (int i = 0; i < TestLoopCount; ++i) {
			float32 timeStep = currentSettings.hz > 0.0f ? 1.0f / currentSettings.hz : float32(0.0f);
			w.SetAllowSleeping(currentSettings.enableSleep);
			w.SetWarmStarting(currentSettings.enableWarmStarting);
			w.SetContinuousPhysics(currentSettings.enableContinuous);
			w.SetSubStepping(currentSettings.enableSubStepping);
			w.Step(timeStep, currentSettings.velocityIterations, currentSettings.positionIterations);

			//record the trail
			mu.lock();
			int j = 0;
			for (auto &b : datasetCopy.allMyBoxes) {
				b2Vec2 pos = b->GetPosition();
				trails[j].push_back(pos);
				++j;

			}
			mu.unlock();
		}

		w.restoreFromSnapshot();
	}
}



TrailPrediction::TrailPrediction() {
	tempOnce = false;
	first_time = true;

	CreateTheWorld(m_world, &dataset);

	//snapshot the world
	m_world->snapshot();


	//create the copiedWorld
	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
	m_world_copy = new b2World(gravity);
	

	
	b2BodyDef bodyDef;
	m_world_copy->CreateBody(&bodyDef);

	CreateTheWorld(m_world_copy, &datasetCopy);
	m_world_copy->snapshot();
	for (auto b : datasetCopy.allMyBoxes) {
		std::vector<b2Vec2> tmpTrail;
		trails.push_back(tmpTrail);
	}


	mBombSpawning = false;

	currentStep = 0;
	currentThreadStep = 0;



}

TrailPrediction::~TrailPrediction() {
	loopThread = false;
}

void TrailPrediction::CreateTheWorld(b2World * worldToAdd, WorldDataSet* dataSet) {
	dataSet->m_fixtureCount = 0;
	b2Timer timer;

	{
		float32 a = 0.5f;
		b2BodyDef bd;
		bd.position.y = -a;
		dataSet->ground = worldToAdd->CreateBody(&bd);


		int32 N = 200;
		int32 M = 10;
		b2Vec2 position;
		position.y = 0.0f;
		for (int32 j = 0; j < M; ++j)
		{
			position.x = -N * a;
			for (int32 i = 0; i < N; ++i)
			{
				b2PolygonShape shape;
				shape.SetAsBox(a, a, position, 0.0f);
				dataSet->ground->CreateFixture(&shape, 0.0f);
				dataSet->m_fixtureCount += 1;
				position.x += 2.0f * a;
			}
			position.y -= 2.0f * a;
		}

	}

	{
		float32 a = 0.5f;
		b2PolygonShape shape;
		shape.SetAsBox(a, a);

		b2Vec2 x(-7.0f, 0.75f);
		b2Vec2 y;
		b2Vec2 deltaX(0.5625f, 1.25f);
		b2Vec2 deltaY(1.125f, 0.0f);

		for (int32 i = 0; i < e_count; ++i)
		{
			y = x;

			for (int32 j = i; j < e_count; ++j)
			{
				b2BodyDef bd;
				bd.type = b2_dynamicBody;
				bd.position = y;

				b2Body* body = worldToAdd->CreateBody(&bd);
				dataSet->allMyBoxes.push_back(body);
				body->CreateFixture(&shape, 5.0f);
				dataSet->m_fixtureCount += 1;
				y += deltaY;
			}

			x += deltaX;
		}
	}

	dataSet->m_createTime = timer.GetMilliseconds();
}


void TrailPrediction::Keyboard(int key)
{
	b2Timer timer;
	switch (key)
	{
	
	case GLFW_KEY_1:
		break;
	case GLFW_KEY_2:
		break;
	case GLFW_KEY_3:
		break;
	case GLFW_KEY_4:
		m_world->snapshot();
		break;
	case GLFW_KEY_5:
		
		m_world->restoreFromSnapshot();
		
		break;
	case GLFW_KEY_R:
		//Create(key - GLFW_KEY_1);
		break;

	case GLFW_KEY_D:
		//DestroyBody();
		break;
	}
	dataset.m_createTime = timer.GetMilliseconds();
}

void TrailPrediction::MouseDown(const b2Vec2& p) {
	mBombSpawning = true;
	mBmbSpawnPos = p;
	if (m_bomb == NULL) {
		m_bomb = createProjectile(mBmbSpawnPos, b2Vec2_zero, m_world);
	}
	else {
		updateProjectile(mBmbSpawnPos, b2Vec2_zero, m_bomb);
	}

#ifndef MULTITHREAD
	if (bombCopied == NULL) {
		bombCopied = createProjectile(mBmbSpawnPos, b2Vec2_zero, m_world_copy);
}
	else {
		updateProjectile(mBmbSpawnPos, b2Vec2_zero, bombCopied);
	}
#endif // !MULTITHREAD



	bombSpawnStep = currentStep;

	tempOnce = true;
}



void TrailPrediction::MouseUp(const b2Vec2& p) {
	float multiplier = 5;
	if (m_bomb != NULL) {
		updateProjectile(mBmbSpawnPos, multiplier*(mBmbSpawnPos - p), m_bomb);
	}

	if (bombCopied != NULL) {
		bombShootStep == currentStep;
	}

#ifndef MULTITHREAD
	if (bombCopied != NULL) {
		updateProjectile(mBmbSpawnPos, multiplier*(mBmbSpawnPos - p), bombCopied);
	}
#endif // !MULTITHREAD
	mBombSpawning = false;
	bombShootStep = currentStep;
}




void TrailPrediction::Step(Settings* settings)
{

	const b2ContactManager& cm = m_world->GetContactManager();
	int32 height = cm.m_broadPhase.GetTreeHeight();

	int32 leafCount = cm.m_broadPhase.GetProxyCount();
	int32 minimumNodeCount = 2 * leafCount - 1;
	float32 minimumHeight = ceilf(logf(float32(minimumNodeCount)) / logf(2.0f));
	g_debugDraw.DrawString(5, m_textLine, "dynamic tree height = %d, min = %d", height, int32(minimumHeight));
	m_textLine += DRAW_STRING_NEW_LINE;


	Test::Step(settings);
	currentStep++;
	//StepCopiedWorld(settings);
	//simulateCopiedWorld(settings);

#ifdef MULTITHREAD
	if (first_time) {
		std::thread threadTemp(threadTest2, std::ref(*m_world_copy));
		threadTemp.detach();
		currentSettings = *settings;
		first_time = false;
	}
#else
	StepCopiedWorld(settings);
	simulateCopiedWorld(settings);
#endif // MULTITHREAD





	//draw the trails
	b2Color color;
	color.Set(1, 1, 1, 1);
	mu.lock();
	for (auto t: trails) {
		for (auto p: t) {

			g_debugDraw.DrawPoint(p, 1, color);
		}
	}
	mu.unlock();



	//drawing data
	g_debugDraw.DrawString(5, m_textLine, "Initialize time = %6.2f ms, fixture count = %d",
		dataset.m_createTime, dataset.m_fixtureCount);
	m_textLine += DRAW_STRING_NEW_LINE;

	//drawing bomb launch and keep the bomb positoin
	if (mBombSpawning) {
		b2Color c;
		c.Set(0.0f, 0.0f, 1.0f);
		g_debugDraw.DrawPoint(mBmbSpawnPos, 4.0f, c);

		c.Set(0.8f, 0.8f, 0.8f);
		g_debugDraw.DrawSegment(m_mouseWorld, mBmbSpawnPos, c);
		if (m_bomb != NULL) {
			updateProjectile(mBmbSpawnPos, b2Vec2_zero, m_bomb);
		}

		mBmbSpawnMousePos = m_mouseWorld;
	}

}


void TrailPrediction::ResetAllBoxes() {
	b2Timer timer;

	if (m_bomb)
	{
		m_world->DestroyBody(m_bomb);
		m_bomb = NULL;
	}

	m_world->restoreFromSnapshot();
	dataset.m_createTime = timer.GetMilliseconds();

}





void TrailPrediction::StepCopiedWorld(Settings* settings) {
	//step the copied world
	float32 timeStep = settings->hz > 0.0f ? 1.0f / settings->hz : float32(0.0f);
	m_world_copy->SetAllowSleeping(settings->enableSleep);
	m_world_copy->SetWarmStarting(settings->enableWarmStarting);
	m_world_copy->SetContinuousPhysics(settings->enableContinuous);
	m_world_copy->SetSubStepping(settings->enableSubStepping);
	m_world_copy->Step(timeStep, settings->velocityIterations, settings->positionIterations);

	//record the trail
	int j = 0;
	for (auto &b : datasetCopy.allMyBoxes) {
		b2Vec2 pos = b->GetPosition();
		trails[j].push_back(pos);
		++j;

	}

}

void TrailPrediction::simulateCopiedWorld(Settings* settings) {


	m_world_copy->snapshot();
	if (bombCopied != NULL && mBombSpawning)
	{
		float multiplier = 5;
		updateProjectile(mBmbSpawnPos, multiplier*(mBmbSpawnPos - m_mouseWorld), bombCopied);
	}

	for (auto &t : trails) {
		t.clear();
	}

	for (int i = 0; i < TestLoopCount; ++i) {
		StepCopiedWorld(settings);


	}

	m_world_copy->restoreFromSnapshot();

}




b2Body* TrailPrediction::createProjectile(const b2Vec2& position, const b2Vec2& velocity, b2World * worldToAdd)
{
	b2Body* bomb;

	b2BodyDef bd;
	bd.type = b2_dynamicBody;
	bd.position = position;
	bd.bullet = true;
	bomb = worldToAdd->CreateBody(&bd);
	bomb->SetLinearVelocity(velocity);

	b2CircleShape circle;
	circle.m_radius = 0.3f;

	b2FixtureDef fd;
	fd.shape = &circle;
	fd.density = 20.0f;
	fd.restitution = 0.0f;

	b2Vec2 minV = position - b2Vec2(0.3f, 0.3f);
	b2Vec2 maxV = position + b2Vec2(0.3f, 0.3f);

	b2AABB aabb;
	aabb.lowerBound = minV;
	aabb.upperBound = maxV;

	bomb->CreateFixture(&fd);
	return bomb;
}

void TrailPrediction::updateProjectile(const b2Vec2& position, const b2Vec2& velocity, b2Body* projectile) {
	projectile->SetLinearVelocity(velocity);
	projectile->SetTransform(position, 0);
}