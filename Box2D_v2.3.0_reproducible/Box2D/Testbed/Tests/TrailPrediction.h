

#ifndef TRAIL_PREDCITION_H
#define TRAIL_PREDCITION_H

#include<vector>
#include <iostream>
#include<Testbed/Framework/Test.h>
#include <thread>
#include <mutex>

using namespace std;

//#define MULTITHREAD


struct WorldDataSet {
	std::vector<b2Body*> allMyBoxes;
	b2Body* ground;
	int32 m_fixtureCount;
	float32 m_createTime;
};

class TrailPrediction : public Test
{
private:

	WorldDataSet dataset;


	b2World *m_world_copy;
	
	
	


	bool tempOnce;



	

	bool first_time;
public:

	

	enum
	{
		e_count = 5
	};

	TrailPrediction();
	~TrailPrediction();
	//Create new boxes and ground in the world.
	//this function will not destroy the old ones
	void CreateTheWorld(b2World * worldToAdd, WorldDataSet* dataSet);

	

	void Keyboard(int key);
	void MouseDown(const b2Vec2& p);
	void MouseUp(const b2Vec2& p);

	void Step(Settings* settings);
	void StepCopiedWorld(Settings* settings);

	void simulateCopiedWorld(Settings* settings);
	static b2Body* createProjectile(const b2Vec2& position, const b2Vec2& velocity, b2World * worldToAdd);

	static void updateProjectile(const b2Vec2& position, const b2Vec2& velocity,  b2Body* projectile);

	static Test* Create()
	{
		return new TrailPrediction;
	}

	void ResetAllBoxes();


};

#endif
